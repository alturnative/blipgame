﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using static System.Threading.Thread;
using static System.Console;

namespace BlipGame
{
    class Program
    {
        private static readonly Dictionary<ConsoleKey, Action> Actions = new()
            {
                {ConsoleKey.LeftArrow,  Left},
                {ConsoleKey.RightArrow, Right},
                {ConsoleKey.UpArrow,    Up},
                {ConsoleKey.DownArrow,  Down}
            };
        private static readonly Random Rng = new Random();
        private static (int left, int top) _target;
        private const int TargetIntervalMs = 10000;
        private static int _score = 0;
        
        static void Main(string[] args)
        {
            CursorVisible = false;

            if (OperatingSystem.IsWindows())
            {
                BufferHeight = WindowHeight;
            }
            
            DisplayIntro();
            RunBlip();
            DisplayScore();
        }
        private static void CentreCursor(int hOffset = 0, int vOffset = 0)
        {
            SetCursorPosition(WindowWidth / 2 + hOffset, WindowHeight / 2 + vOffset);
        }
        private static void CentreCursor(string input, int vOffset = 0)
        {
            CentreCursor(hOffset: - input.Length / 2, vOffset);
        }
        private static void DisplayIntro()
        {
            string str = "Welcome to the Matrix...";
            StringBuilder builder = new StringBuilder();

            CentreCursor(vOffset: -1);
            Sleep(200);

            for (int i = 0; i < str.Length; i += 2)
            {
                Sleep(100);
                SetCursorPosition(CursorLeft - i - 1, CursorTop);
                builder.Append(str.Substring(i, 2));

                Write(builder);
            }

            string intro = "Use the arrow keys to move the blip";

            CentreCursor(intro, 3);
            Sleep(800);

            Write(intro);
            Sleep(300);

            ReadKey(true);
        }
        private static void RunBlip()
        {
            Clear();
            CentreCursor();
            WriteInPlace("#");
            
            StartTargetGeneratorTimer(out var targetGeneratorTimer);
            
            ConsoleKey key = ReadKey(true).Key;
            
            while (!key.Equals(ConsoleKey.Escape))
            {
                if (Actions.ContainsKey(key))
                {
                    ClearChars();
                    
                    Actions[key]();
                    
                    AvoidBufferNewLine(key);
                    
                    CheckTarget(ref targetGeneratorTimer);
                    WriteInPlace("#");
                }
                
                key = ReadKey(true).Key;
            }
            
            // Once we're done with the game, stop the timer to prevent further targets being generated later on
            targetGeneratorTimer.Dispose();
        }

        private static void StartTargetGeneratorTimer(out Timer timer)
        {
            timer = new Timer(GenerateTarget, null, 0, 10000);
        }

        private static void AvoidBufferNewLine(ConsoleKey key)
        {
            // When a character is printer to the final line in the buffer, a new line is generated which shifts the
            // rest of the display up one line. Hacky solution is to avoid settling on that cursor position in the first
            // place by performing whatever movement got us there one more time, effectively skipping over that position
            
            if (GetCursorPosition() == (WindowWidth - 1, WindowHeight - 1))
            {
                Actions[key]();
            }
        }

        private static void WriteInPlace(string input)
        {
            (int left, int top) = GetCursorPosition();
            Write(input);
            SetCursorPosition(left, top);
        }
        private static void ClearChars(int offset = 0, int length = 1)
        {
            if (length < 1)
            {
                throw new ArgumentException("Length must be greater than zero.");
            }
            
            CursorLeft += offset;
            
            WriteInPlace(new String(' ', length));
        }
        private static void Left()
        {
            CursorLeft = CursorLeft == 0
                ? WindowWidth - 1
                : CursorLeft - 1;
        }
        private static void Right()
        {
            CursorLeft = CursorLeft == WindowWidth - 1
                ? 0
                : CursorLeft + 1;
        }
        private static void Up()
        {
            CursorTop = CursorTop == 0
                ? WindowHeight - 1
                : CursorTop - 1;
        }
        private static void Down()
        {
            CursorTop = CursorTop == WindowHeight - 1
                ? 0
                : CursorTop + 1;
        }
        private static void GenerateTarget(Object o)
        {
            (int left, int top) blip = GetCursorPosition();
            
            if (_target != blip)
            {
                SetCursorPosition(_target.left, _target.top);
                WriteInPlace(" ");
            }
            
            _target = (Rng.Next(0, WindowWidth - 1), Rng.Next(0, WindowHeight - 1));
            
            if (_target == (WindowWidth - 1, WindowHeight - 1) || _target == blip)
            {
                GenerateTarget(o);
            }

            SetCursorPosition(_target.left, _target.top);
            WriteInPlace("X");
            
            SetCursorPosition(blip.left, blip.top);
        }
        private static void DisplayScore()
        {
            Clear();
            string output = $"Your score is: {_score:N0}";
            CentreCursor(output);
            Write(output);
            ReadKey(true);
        }
        private static void CheckTarget(ref Timer timer)
        {
            if (GetCursorPosition() == _target)
            {
                _score++;
                timer.Change(0, TargetIntervalMs);
                Sleep(5);
            }
        }
    }
}